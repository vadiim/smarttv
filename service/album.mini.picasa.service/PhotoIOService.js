
var Service = require('webos-service');
var service = new Service('album.mini.picasa.service');
var fs = require('fs'),
	request = require('request');

service.register('download', function(message) {
	var model = message.payload,
		fileName = model.fileName,
		url = model.url;

	request.head(url, function(err, res, body) {
		if (err) {
			message.respond({
				returnValue: false,
				errorCode: 1,
	            errorText: err.message
			});
		} else {
			request(url).pipe(fs.createWriteStream(fileName)).on('error', function(err) {
				message.respond({
					returnValue: false,
					errorCode: 2,
		            errorText: err.message
				});
			}).on('close', function() {				
				model['url'] = fs.realpathSync(fileName);
				message.respond({
					returnValue: true,
					model: model
				});					
			});			
		}
	});
});

service.register('remove', function(message) {
	var model = message.payload,
		fileName = model.fileName;
	
	fs.unlink(fileName, function(err) {
		if (err) {
			message.respond({
				returnValue: false,
				errorCode: 3,
				errorText: err.message
			});
		} else {
			message.respond({
				returnValue: true
			});
		}
	});
});
