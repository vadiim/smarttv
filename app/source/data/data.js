enyo.kind({
	name: 'picasa.Global',
	/**
	 * Define global constant 
	 */
	statics: {
		QueryKind: 'album.mini.picasa:1',
		Owner: 'album.mini.picasa',
		VisibleColumnCnt: 6,
		VisibleRowCnt: 3,
		ItemHeight: 308
	}	
});

enyo.kind({
	name: 'picasa.Source',
	kind: 'enyo.JsonpSource',
	urlRoot: 'https://picasaweb.google.com/data/feed/tiny/featured',
	/**
	 * Fetch data from Picasa Web Server
	 */
	fetch: function (rec, opts) {
		opts.params = enyo.clone(rec.params);
		
		opts.params.alt = 'jsonm';
		opts.params.kind = 'photo';
		opts.params.slabel = 'featured';
		opts.params['max-results'] = 30;
		
		this.inherited(arguments);
	}
});

new picasa.Source({ name: 'picasa' });

enyo.kind({
	name: 'picasa.ImageModel',
	kind: 'enyo.Model',
	options: {
		parse: true
	},
	/**
	 * Parse a fetched data 
	 */
	parse: function (data) {
		if (data.media) {
			var media = data.media;
			
			data.authorName = data.author[0].name;
			data.thumbnail = media.thumbnail[media.thumbnail.length - 1].url;
			data.url = media.content[0].url;
		}

		return data;
	}
});