enyo.kind({
	name: 'picasa.ImageList.Collection',
	kind: 'enyo.Collection',
	model: 'picasa.ImageModel',
	source: 'picasa',	
	options: {
		parse: true
	},
	published: {
		album: null,
		topItemIndex: 0,
		downloading: false
	},
	events: {
		onMessageChanged: ''
	},
	/**
	 * Get LunaService to find data from DB8
	 */
	getFindDBService: function () {
		if (this.$.findDB === undefined) {
			this.createComponent({
				name: 'findDB', 
				kind: 'enyo.LunaService', 
				service: 'luna://com.palm.db/', 
				method: 'find',
				onResponse: 'handleFindResponse', 
				onError: 'handleError'
			}, {owner: this});
		}
		return this.$.findDB;
	},
	/**
	 * Get LunaService to store Kind in DB8
	 */
	getPutKindDBService: function () {
		if (this.$.putKindDB === undefined) {
			this.createComponent({
				name: 'putKindDB', 
				kind: 'enyo.LunaService', 
				service: 'luna://com.palm.db/', 
				method: 'putKind',
				onError: 'handleError'
			}, {owner: this});
		}
		return this.$.putKindDB;		
	},
	/**
	 * Get LunaService to delete Kind in DB8
	 */
	getDelKindDBService: function () {
		if (this.$.delKindDB === undefined) {
			this.createComponent({
				name: 'delKindDB', 
				kind: 'enyo.LunaService', 
				service: 'luna://com.palm.db/', 
				method: 'delKind',
				onError: 'handleError'
			}, {owner: this});
		}
		return this.$.delKindDB;		
	},
	/**
	 * Get LunaService to put data in DB8
	 */
	getPutDBService: function () {
		if (this.$.putDB === undefined) {
			this.createComponent({
				name: 'putDB', 
				kind: 'enyo.LunaService', 
				service: 'luna://com.palm.db/', 
				method: 'put',
				onError: 'handleError'
			}, {owner: this});
		}
		return this.$.putDB;		
	},	
	/**
	 * Get LunaService to delete data from DB8
	 */
	getDelDBService: function () {
		if (this.$.delDB === undefined) {
			this.createComponent({
				name: 'delDB', 
				kind: 'enyo.LunaService', 
				service: 'luna://com.palm.db/', 
				method: 'del',
				onResponse: 'handleDelResponse', 
				onError: 'handleError'
			}, {owner: this});
		}
		return this.$.delDB;		
	},
	/**
	 * Get LunaService to download image file.
	 * It is a user-defined Node.js Service and source files are located in /services/album.mini.picasa.service folder
	 */
	getPhotoDownloadService: function () {
		if (this.$.photoDownload === undefined) {
			this.createComponent({
				name: 'photoDownload',
				kind: 'enyo.LunaService',
				service: 'luna://album.mini.picasa.service/',
				method: 'download',
				onResponse : 'handleDownloadResponse', 
				onError : 'handleError'
			}, {owner: this});
		}
		return this.$.photoDownload;
	},
	/**
	 * Get LunaService to delete local image file 
	 */
	getPhotoRemoveService: function () {
		if (this.$.photoRemove === undefined) {
			this.createComponent({
				name: 'photoRemove',
				kind: 'enyo.LunaService',
				service: 'luna://album.mini.picasa.service/',
				method: 'remove',
				onError : 'handleError'
			}, {owner: this});
		}
		return this.$.photoRemove;
	},
	create: enyo.inherit(function (sup) {
		return function () {
			sup.apply(this, arguments);
			this.createKindDB();
		};
	}),	
	/**
	 * Create Kind in DB8 to store photo information
	 * albumTypes are FE(Featured Photos), FA(Favorites), DW(Downloaded Photos)
	 */
	createKindDB: function () {
		var params = { 
			'id': picasa.Global.QueryKind,
			'owner': picasa.Global.Owner,
			'indexes': [{'name': 'album', 'props': [{'name': 'albumType'}]}]
		};
			
		this.getPutKindDBService().send(params);
	},
	/**
	 * Delete Kind in DB8
	 */
	deleteKindDB: function () {
		this.getDelKindDBService().send({'id': picasa.Global.QueryKind});
	},
	/**
	 * start-index used in the picasa web begins from 1
	 */
	topItemIndexChanged: function (oldValue, newValue) {
		if (this.album === 'FE') {			
			if (oldValue < newValue) {
				if (this.length < (newValue + picasa.Global.VisibleColumnCnt * picasa.Global.VisibleRowCnt)) {
					this.fetch({
						'start-index': this.length + 1
					});
				}
			}
		}
	},
	albumChanged : function (oldValue, newValue) {
		this.findAlbum(newValue);
	},
	/**
	 * Fetch photo data from Picasa Server(Featured Photos) or
	 * DB8(Favorites, Downloaded Photos) 
	 */
	findAlbum: function (albumType) {
		enyo.Signals.send('onDeselectGridList');
		this.empty({ destroy: true });
		
		if (/FA|DW/.test(albumType)) {
			this.fetchDBPhotos(albumType);
		} else {
			this.fetch();			
		}
	},
	fetch : function (opts) {
		this.params = opts || {};
		return this.inherited(arguments);
	},
	parse : function (data) {
		if (data instanceof Array) 
			return data;
		
		return data && data.feed && data.feed.entry;
	},
	/**
	 * Fetch photo data from DB8
	 */
	fetchDBPhotos: function (albumType) {
		var params = {
			'query': {
				'from': picasa.Global.QueryKind,
				'where': [ {
					'prop': 'albumType',
					'op': '=',
					'val': albumType
				} ]
			},
			'count': true
		};
		this.getFindDBService().send(params);
	},
	handleFindResponse: function (request, response) {
		this.add(response.results, 'merge');
	},
	/**
	 * Store the necessary data required to display photos in album panel
	 */
	registPhotos: function (items, callback) {
		items = items || [];
		if (items.length == 0) 
			return;

		var params = {
			'objects': []
		};
		
		for (var i = 0, l = items.length; i < l; i++) {
			params.objects.push(this._mixinPhotoInfo(items[i], {
				'albumType': 'FA'
			}));
		}
		
		var service = this.getPutDBService(); 
		service.onComplete = 'handleRegistComplete';
		service.send(params);
	},
	/**
	 * Delete the selected photo
	 */
	removePhotos: function (items) {
		items = items || [];
		if (items.length == 0) 
			return;

		var params = {
			'ids': [],
			'purge': true
		};
		
		for (var i = 0, l = items.length; i < l; i++) {
			params.ids.push(items[i]._id);
		}

		this._items = items;
		this.getDelDBService().send(params);
	},
	/**
	 * Success handler for deleting photos in ablum: favorites, downloaded photos.
	 * If album is the downloaded photo album, delete files from local storage.
	 */
	handleDelResponse: function (request, response) {
		this.doMessageChanged({
			'message': $L('Remove Completed')
		});
		
		var album = this.get('album');

		if ('DW' === album) {
			var items = this._items,
				service = this.getPhotoRemoveService();
			
			for (var i = 0, l = items.length; i < l; i++) {
				service.send(items[i]);
			}
		}

		this.findAlbum(album);
		delete this._items;
	},
	/**
	 * Error handler commonly used 
	 */
	handleError: function (request, response) {
		this.doMessageChanged({
			'message': response.errorText
		});
	},
	handleRegistComplete: function (request, response) {
		if (response.returnValue) {
			enyo.Signals.send('onDeselectGridList');
			this.doMessageChanged({
				'message': $L('Regist Completed')
			});
		}
	},
	
	downloadPhoto: function (v) {
		this.set('downloading', true);
		this.getPhotoDownloadService().send(this._mixinPhotoInfo(v, {
			'albumType': 'DW'
		}));
	},
	/**
	 * Get common and optional mixed data
	 */
	_mixinPhotoInfo: function (v, opts) {
		return enyo.mixin({
			'_kind': picasa.Global.QueryKind,
			'fileName': v.gphoto$id + '_' + +new Date,
			'gphoto$id': v.gphoto$id,
			'title': v.title,
			'authorName': v.authorName,
			'thumbnail':v.thumbnail,
			'url': v.url
		}, opts);
	},
	/**
	 * Success handler for downloading image file.
	 * After downloading the image successfully, store that information in the DB8 
	 */
	handleDownloadResponse: function (request, response) {
		var service = this.getPutDBService();
		service.onComplete = 'handleDownloadComplete';			
		service.send({'objects': [response.model]});
	},
	handleDownloadComplete: function (request, response) {
		this.set('downloading', false);
		
		if (response.returnValue) {
			this.doMessageChanged({
				'message': $L('Download Completed')
			});
		}
	}
});