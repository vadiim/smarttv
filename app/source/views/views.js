/**
 * Main View
 * It defines entire layout of Mini Picasa App and collection to get photo files.
 */
enyo.kind({
	name: 'picasa.MainView',
	classes: 'moon enyo-fit',
	handlers: {
		onItemSelected: 'handleItemSelected',
		onMessageChanged: 'handleMessageChanged'
	},
	components: [
        {kind: 'moon.Panels', pattern: 'activity', classes: 'enyo-fit', components: [
	        {kind: 'picasa.AlbumPanel'},
	        {kind: 'picasa.ViewPanel'}
		]},
		{kind: 'moon.Popup', style: 'text-align: center;'}
	],	
	create: enyo.inherit(function (sup) {
		return function () {
			sup.apply(this, arguments);
			
			this.createComponent({
				kind: 'picasa.ImageList.Collection'
			}, { owner: this });
			
			this.$.albumPanel.set('collection', this.$.collection);
			this.$.viewPanel.set('collection', this.$.collection);
		};
	}),
	handleMessageChanged: function (inSender, inEvent) {
		this.$.popup.set('content', inEvent.message || $L('Completed'));
		this.$.popup.show();
		
		this.startJob('showMessagePopup', function () {
			this.$.popup.hide();
		}, 1500);
	},
	/**
	 * Event handler for selecting item on album panel(picasa.AlbumPanel)
	 */
	handleItemSelected: function (inSender, inEvent) {
		this.$.viewPanel.set('model', inEvent.model);
		this.$.panels.next();
	}
});

enyo.kind({
	name: 'picasa.AlbumPanel',
	kind: 'moon.Panel', 
	title:'Picasa Web', 
	smallHeader: true, 	
	published: {
		collection: ''
	},
	events: {
		onItemSelected: ''
	},
	headerComponents: [
		{kind: 'moon.Spinner', content: $L('Loading'), name: 'spinner'},
		{kind: 'moon.ToggleButton', content: $L('Selection'), name: 'selectionToggle'},
		{kind: 'moon.Button', name: 'favoriteBtn', content: $L('Regist'), ontap: 'registPhotos'},
		{kind: 'moon.Button', name: 'removeBtn', content: $L('Remove'), ontap: 'removePhotos'}
	], 
	components: [
	    {kind: 'Signals', onDeselectGridList: 'deselectGridList'},         
		{kind: 'FittableColumns', fit: true, components: [
			{
				name: 'category',
				kind: 'enyo.Group',
				classes: 'moon-6h', 
				components: [
					{kind: 'moon.SelectableItem', content: $L('Featured Photos'), selected: true, albumType: 'FE'},
					{kind: 'moon.SelectableItem', content: $L('Favorites'), albumType: 'FA'},
					{kind: 'moon.SelectableItem', content: $L('Downloaded Photos'), albumType: 'DW'}
				],
				onActivate: 'changeList'
			},
			{name: 'gridList', fit: true, multipleSelection: true, spacing: 20, minWidth: 180, minHeight: 270, kind: 'moon.DataGridList', ontap: 'itemSelected',
				published: {
					topItemIndex: 0
				},
				scrollerOptions: { 
					kind: 'moon.Scroller', 
					vertical: 'scroll', 
					horizontal: 'hidden', 
					spotlightPagingControls: true,
					onScroll: 'pageHandler'
				},
				pageHandler: function (inSender, inEvent) {
					var index = Math.ceil(inEvent.scrollBounds.top / (picasa.Global.ItemHeight + this.spacing)) * picasa.Global.VisibleColumnCnt;
					
					if (this.collection.at(index)) {
						this.set('topItemIndex', index);
					}
	            },
				components: [
				    {kind: 'picasa.GridPhotoItem', imageSizing: 'cover'}
			    ]
			}
		]}
	],
	bindings: [
        {from: '$.gridList.topItemIndex', to: 'collection.topItemIndex'},
   		{from: 'collection', to: '$.gridList.collection'},
		{from: 'collection.status', to:'$.spinner.showing', transform: function (value) {
			return this.collection.isBusy();
		}},
		{from: '$.selectionToggle.value', to:'$.gridList.selection', transform: function (value) {
			this.deselectGridList();
			return value;
		}}		
   	],
   	/**
   	 * Change Album (FE: Featured Photos, FA: Favorites, DW: Downloaded Photos)
   	 */
	changeList: function (inSender, inEvent) {
		if (inEvent.originator.getActive()) {
			var album = this.$.category.active.get('albumType');	
			
			this.$.favoriteBtn.setShowing('FE' === album);
			this.$.removeBtn.setShowing(/FA|DW/.test(album));

			this.collection.set('album', album);
		}
	},
	/**
	 * Deselect all selected grid items. 
	 */
	deselectGridList: function () {
		this.$.gridList.deselectAll();
	},
	/**
	 * Save information(url, title, author, ...) of selected photos to DB8 
	 */
	registPhotos: function (inSender, inEvent) {
		var list = this.$.gridList.selected() || [];
		if (list.length) {
			var items = [];
			
			for (var i = 0, l = list.length; i < l; i++) {
				items.push(list[i]['attributes']);
			}
			
			this.collection.registPhotos(items);
		}
	},
	/**
	 * Delete information from DB8.  
	 * If photo was downloaded, it will be removed from local storage too.
	 */
	removePhotos: function (inSender, inEvent) {
		var list = this.$.gridList.selected() || [];
		if (list.length) {
			var items = [];
			
			for (var i = 0, l = list.length; i < l; i++) {
				items.push(list[i]['attributes']);
			}
			
			this.collection.removePhotos(items);
		}
	},
	/**
	 * Fire ItemSelected event.
	 */
	itemSelected: function (inSender, inEvent) {
		if (!this.$.gridList.selection && inEvent.model) {
			this.doItemSelected({
				model: inEvent.model
			});
		}
	}
});

enyo.kind({
	name: 'picasa.GridPhotoItem',
	kind: 'moon.GridListImageItem',
	mixins: ['moon.SelectionOverlaySupport'],
	selectionOverlayVerticalOffset: 35,
	bindings: [
		{from: 'model.title', to: 'caption'},
		{from: 'model.authorName', to: 'subCaption'},
		{from: 'model.thumbnail', to: 'source'}
	]
});

enyo.kind({
	name: 'picasa.ViewPanel',
	kind: 'moon.Panel', 
	smallHeader: true,
	layoutKind: 'FittableColumnsLayout',
	headerComponents: [
   		{kind: 'moon.Spinner', content: $L('Downloading'), name: 'spinner'},
   		{kind: 'moon.Button', name: 'downloadBtn', content: $L('Download'), ontap: 'downloadPhoto'}
   	],
	components: [
		{kind: 'moon.Image', name: 'image', fit: true, sizing: 'contain'}
	],
	bindings: [
	    {from: 'collection.downloading', to: '$.spinner.showing'},
		{from: 'model.title', to: 'title'},
		{from: 'model.authorName', to: 'titleBelow'},
		{from: 'model.url', to: '$.image.src'},
		{from: 'model.albumType', to: '$.downloadBtn.showing', transform: function (value) {
			return value != 'DW';
		}}		
	],
	downloadPhoto: function (inSender, inEvent) {
		this.collection.downloadPhoto(this.model['attributes']);
	}
});